$(document).ready(function () {

    $("#duplicatesSection").hide();
    $("#uniqueSection").hide();

    $("#mainForm").on("submit", function(e) {
        e.preventDefault();

        $("#duplicatesSection").show();
        $("#uniqueSection").show();

        var selectedColumn = $('#columnName option:selected').text();
        var payload = { columnName: selectedColumn };

        $.when(
            $.ajax({
                url: "http://localhost:8080/duplicates",
                data: payload,
                dataType: "json"
            }),
            $.ajax({
                url: "http://localhost:8080/unique",
                data: payload,
                dataType: "json"
            })
        ).done(function(duplicates, unique) {
            fillSection("#duplicatesSection", duplicates[0]);
            fillSection("#uniqueSection", unique[0]);
        });
    });
});

function fillSection(sectionId, data) {
    if (data.length === 0) {
        $(sectionId).find("table").hide();
        $(sectionId).find("span").show();
    }
    else {
        $(sectionId).find("span").hide();

        $(sectionId).find("table").show();
        $(sectionId).find("tbody").html("");

        data.forEach(function(row) {
            var newRow =
                "<tr>" +
                    "<td>" + row.id + "</td>" +
                    "<td>" + row.column1 + "</td>" +
                    "<td>" + row.column2 + "</td>" +
                    "<td>" + row.column3 + "</td>" +
                    "<td>" + row.column4 + "</td>" +
                "</tr>";
            $(sectionId).find("tbody").append(newRow);
        });
    }
}