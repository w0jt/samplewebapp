package com.example.controllers;

import com.example.entities.SomeEntity;
import com.example.repositories.SomeEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller("/")
public class SomeEntityController {

    @Autowired
    private SomeEntityRepository someEntityRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String mainView() {
        return "mainView";
    }

    @RequestMapping(value = "/duplicates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public List<SomeEntity> getDuplicates(@RequestParam("columnName") String columnName) {
        return someEntityRepository.getWithDuplicateColumnValue(columnName);
    }

    @RequestMapping(value = "/unique", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public List<SomeEntity> getUniqueResults(@RequestParam("columnName") String columnName) {
        return someEntityRepository.getWithUniqueColumnValue(columnName);
    }
}
