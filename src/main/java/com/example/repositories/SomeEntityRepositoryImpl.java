package com.example.repositories;

import com.example.entities.SomeEntity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class SomeEntityRepositoryImpl implements SomeEntityRepository {

    private static final String SELECT_UNIQUE_TEMPLATE;
    private static final String SELECT_DUPLICATES_TEMPLATE;
    private static final Set<String> allowedColumnNames;
    static {
        SELECT_UNIQUE_TEMPLATE =    "select s.id, s.column1, s.column2, s.column3, s.column4\n" +
                                    "from some_entity s\n" +
                                    "where (select count(*) from some_entity p where p.:columnName = s.:columnName) = 1";

        SELECT_DUPLICATES_TEMPLATE =    "select s.id, s.column1, s.column2, s.column3, s.column4\n" +
                                        "from some_entity s\n" +
                                        "where (select count(*) from some_entity p where p.:columnName = s.:columnName) > 1";

        allowedColumnNames = new HashSet<>();
        allowedColumnNames.addAll(Arrays.asList("column1", "column2", "column3", "column4"));
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<SomeEntity> getWithDuplicateColumnValue(String columnName) {
        String finalQuery = SELECT_DUPLICATES_TEMPLATE.replace(":columnName", columnName);
        return getResultList(finalQuery, columnName);
    }

    @Override
    public List<SomeEntity> getWithUniqueColumnValue(String columnName) {
        String finalQuery = SELECT_UNIQUE_TEMPLATE.replace(":columnName", columnName);
        return getResultList(finalQuery, columnName);
    }

    private List<SomeEntity> getResultList(String finalQuery, String columnName) {
        if (allowedColumnNames.contains(columnName)) {

            List<Map<String, Object>> queryResultsList = jdbcTemplate.queryForList(finalQuery);
            List<SomeEntity> entityList = new ArrayList<>();

            for (Map<String, Object> queryResult : queryResultsList) {
                SomeEntity entity = new SomeEntity();
                entity.setId((Long) queryResult.get("id"));
                entity.setColumn1((String) queryResult.get("column1"));
                entity.setColumn2((String) queryResult.get("column2"));
                entity.setColumn3((String) queryResult.get("column3"));
                entity.setColumn4((Long) queryResult.get("column4"));

                entityList.add(entity);
            }

            return entityList;
        }
        else {
            return new ArrayList<>();
        }
    }
}
