package com.example.repositories;

import com.example.entities.SomeEntity;

import java.util.List;

public interface SomeEntityRepository {

    List<SomeEntity> getWithDuplicateColumnValue(String columnName);

    List<SomeEntity> getWithUniqueColumnValue(String columnName);
}
