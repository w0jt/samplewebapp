package com.example.entities;

public class SomeEntity {

    private Long id;
    private String column1;
    private String column2;
    private String column3;
    private Long column4;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public String getColumn2() {
        return column2;
    }

    public void setColumn2(String column2) {
        this.column2 = column2;
    }

    public String getColumn3() {
        return column3;
    }

    public void setColumn3(String column3) {
        this.column3 = column3;
    }

    public Long getColumn4() {
        return column4;
    }

    public void setColumn4(Long column4) {
        this.column4 = column4;
    }
}
