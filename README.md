Aplikacja została napisana w Springu z użyciem silnika szablonów Thymeleaf. Jako że jest to standardowy projekt Maven'owy, wszelkie zależności pobierane są automatycznie.

Nieznacznie zmieniony skrypt (o nazwie data.sql) tworzący tabelę wykorzystywaną w aplikacji (oraz wypełniający ją przykładowymi danymi) znajduje się w katalogu src/main/resources

Dane konieczne do nawiązania połączenia z bazą danych znajdują się w pliku /src/main/resources/application.properties